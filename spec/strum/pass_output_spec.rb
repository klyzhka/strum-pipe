# frozen_string_literal: true

RSpec.describe "Pass output" do
  it "has a version number" do
    expect(Strum::Pipe::VERSION).not_to be nil
  end

  # rubocop: disable Metrics/BlockLength
  [
    { name: "pass integer values", value1: 1, value2: 2, value3: 3 },
    { name: "pass string values", value1: "value 1", value2: "value 2", value3: "value 3" },
    { name: "pass boolean values", value1: true, value2: false, value3: true }
  ].each do |params|
    describe params[:name] do
      let(:first_service_class) do
        Class.new do
          include Strum::Service
          define_method(:call) do
            output(value2: params[:value2])
          end
        end
      end

      let(:second_service_class) do
        Class.new do
          include Strum::Service
          define_method(:call) do
            output(value3: params[:value3])
          end
        end
      end

      it "success without block" do
        expect(
          Strum::Pipe.call(first_service_class,
                           second_service_class,
                           input: { value1: params[:value1] })
        ).to eq({ value1: params[:value1], value2: params[:value2], value3: params[:value3] })
      end

      it "success with block" do
        expect(
          Strum::Pipe.call(first_service_class,
                           second_service_class,
                           input: { value1: params[:value1] }) do |m|
            m.success { |result| result }
            m.failure { |errors| errors }
          end
        ).to eq({ value1: params[:value1], value2: params[:value2], value3: params[:value3] })
      end

      it "success without block without input" do
        expect(
          Strum::Pipe.call(first_service_class,
                           second_service_class)
        ).to eq({ value2: params[:value2], value3: params[:value3] })
      end

      it "wrong with block without input" do
        expect(
          Strum::Pipe.call(first_service_class,
                           second_service_class) do |m|
            m.success { |result| result }
            m.failure { |errors| errors }
          end
        ).to eq({ value2: params[:value2], value3: params[:value3] })
      end
    end
  end
  # rubocop: enable Metrics/BlockLength
end
