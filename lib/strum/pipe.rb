# frozen_string_literal: true

require "strum/pipe/version"
require "strum/service"

module Strum
  # Strum Pipe
  class Pipe
    include Strum::Service
    class Error < StandardError; end

    def self.call(*units, args: {}, input: {}, unit_input: {}, &block)
      pipe_payload = if input.is_a?(Hash) && unit_input.is_a?(Hash)
                       unit_input.merge(input)
                     else
                       input
                     end
      new(pipe_payload, args).execute(*units, input, unit_input, &block)
    end

    def execute(*units, pipe_inputs, pipe_unit_inputs, &block) # rubocop: disable Metrics/PerceivedComplexity, Metrics/MethodLength, Metrics/CyclomaticComplexity, Metrics/AbcSize,  Style/CommentedKeyword
      audit
      yield(self) if valid? && block_given?
      output(pipe_inputs)
      continue = true
      pipe_context = self
      while (unit = units.shift) && valid? && continue
        service, service_params = unit
        service_params ||= {}
        raise Strum::Error, "Unit options must be a Hash" unless service_params.is_a?(Hash)

        service_input = service_params[:input] || {}
        result_arg_key = service_params[:to]
        unit_args = case service_params[:args]
                    when Hash
                      service_params[:args]
                    when Symbol, String
                      { service_params[:args] => service_params[:args] }
                    else
                      {}
                    end
        clean_output = service_params[:clean_output] || false
        unit_payload = if !clean_output &&
                          service_input.is_a?(Hash) &&
                          pipe_unit_inputs.is_a?(Hash) &&
                          output_value.is_a?(Hash)
                         pipe_unit_inputs.merge(service_input).merge(output_value)
                       else
                         output_value
                       end

        service.public_send(:call, unit_payload, args.merge(unit_args)) do |m|
          pipe_context.service_handlers[:on].each do |key, _handler|
            m.on(key) { |r| hook(key, r) }
          end

          pipe_context.service_handlers[:success].each do |key, _handler|
            next unless key

            m.success(key) do |result|
              output(key, result)
              continue = false
            end
          end

          m.success do |result|
            if result_arg_key
              pipe_context.inputs[result_arg_key] = result
            elsif !clean_output && result.is_a?(Hash) && output_value.is_a?(Hash)
              pipe_context.output(pipe_context.output_value.merge(result))
            elsif result
              pipe_context.output(result)
            end
          end

          m.failure { |errors| add_errors(errors) }
        end
      end
      valid? ? valid_result(&block) : invalid_result(&block)
    end
  end
end
